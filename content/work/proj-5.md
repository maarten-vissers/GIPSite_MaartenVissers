### Bekijk hieronder mijn vergelijking tussen Wordpress en Hugo en vergeet zeker de structuur van Hugo niet te bezien!

1) Wordpress vs (Go)Hugo
===================
*Welkom op mijn vergelijking tussen Wordpress en GoHugo. WordPress is een online, **open source website creation tool** geschreven in PHP. Hugo is een **static site generator**. Dit betekent dat, in tegenstelling tot systemen zoals  WordPress, Ghost and Drupal, die op uw web server draaien elke keer hun page bouwen als een bezoeker er op klikt. Bij Hugo is het al gebouwd bij het maken van uw content en hoeft dit dus maar één keer te gebeuren.*

Hugo en Wordpress lijken ergens wel op elkaar. Je maakt bij beide eerst een site, kiest een thema en maakt post. Wordpress daarentegen is stukken gemakkelijker dan Hugo. Bij WordPress doe je alles via een soort van GUI, in tegenstelling tot Hugo waar je alles met code doet. Zowel in je site als in de command prompt bij het maken van die code b.v.: Site maken

	>hugo new site GipSite


: Test.md bestand maken in de map "post"

	>hugo new post\test.md

Zelf vind ik Hugo het meest interessante, het is meer IT-gerelateerd. In het begin is het heel moeilijk, je moet ergens aan beginnen waar je niets van weet. Uiteindelijk is iedereen er wel in geslaagd door middel van zelfstudie en hulp van vrienden er te geraken. Dat is wat wij later ook zullen moeten doen, want dan is er geen leeraar die ons helpt. De eerste weken heb ik problemen gehad met Hugo omdat ik vaak te moeilijke thema's had, maar het is net daardoor dat ik het nu snap. Ik snap nu bijvoorbeeld hoe de content verbonden is met de single.html, deze zorgt namelijk voor de opmaak van de content. Ik weet ook veel meer over partials. Iets waar ik voor Hugo bijna niets van wist, ken ik nu heel goed.

Bij Hugo begin je met een site te maken en vervolgens een thema te kiezen. De moeilijkste stap is het thema configuren en alle instelling in de config.toml te doen werken. Vervolgens kan je beginnen met post, zoals de code hierboven, deze kan je dan bewerken met behulp van lay-outs en dergelijke. In WordPress is het heel gemakkelijk. Je hoeft enkel te klikken op: "nieuwe site maken" of "nieuwe post maken" en je krijgt een scherm waar alles duidelijk is.  


Kleine vergelijking
------------
| Aspecten                  |		   Wordpress |				Hugo  |
| :------- | ----: | :---:	|
| **Moeilijkheidsgraad**	|		 Gemakkelijk |		 Gevorderd    |
| **Op elke pc bereikbaar** |			    Ja   |				 Ja   |
| **Git**					|			  Nee    |				  Ja  |
| **Snel**				    |			   Ja    |				 Nee  |
| **Bouwen (build)**		|  Klik op pagina    |			Eenmalig  |


De Hugo site wordt gehost door github pages. In deze directory wordt je public map van hugo geplaatst.



2) Directory-Structuur
===========


Mijn Hugo mappen en bestanden
-------

 - Content
 - Layouts
 - Static
 - Config.toml

 Alles komt van verchillende thema's buiten content, die heb ik zelf geschreven.Vervolgens een woordje uitleg.

Content
------
In de content staan jouw markdown (.md) bestanden, bv. giptaken. Hier schrijf je markdown code die later in de layouts wordt omgezet in html. Wat ik ga doen is hier twee mappen maken: giptaken en stageverslagen.

Layouts
------
Hier worden je markdown bestanden omgezet naar html-bestanden, zoals voordien vermeld. Zoal ik al zei ging ik twee mappen maken, dat wilt dus zeggen dat ik bij layouts eventueel ook twee dezelfde mappen kan maken en ze allebei een andere opmaak geven door middel van de single.
Ik geef ze dezelfde opmaak, dus maak ik gebruik van een _default map met maar één single in. Bij layouts kan je ook de thema' mixen door er partials van andere thema's aan toe te voegen.

Static
-------
Css, fonts, images, javascript...? Dat behoort hier! Vergeet niet bij het mixen van de thema's ook de css en dergelijke van het andere thema hier te plaatsen!

Config.toml
-------
De configuratie gebeurt hier. Gegevens van uw site, uw account of beschrijvingen, namen van knoppen en dergelijke, die geef je hier mee!

*De config en de layout samen doen werken vond ik zeer moeilijk. Ook een tweede thema in de site mengen was geen gemakkelijke opdracht, maar ze zijn wel gelukt!*

