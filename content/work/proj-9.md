![analyse](img/work/proj-9/img1.jpg)
![analyse](img/work/proj-9/img2.jpg)
### Ook zoveel interesse in voetbal? Voer deze code uit in mySqlWorkbench en stem op jouw favoriete club!

**Maak je database en stel het in als standaarddatabase**  
create schema gip_taak_08_VissersMaarten;  
use gip_taak_08_VissersMaarten;

**Maak de tabel Users**  
create table User(  
	id int not null auto_increment primary key,  
    name text not null,  
    functie text not null,  
    email text not null,  
    password text not null  
    );
	
**Maak de tabel Users**  
create table Votes(  
	id int not null auto_increment primary key,  
    user_id int not null,  
    club_id int not null,  
    score int not null  
    );
    
**Maak de tabel Club**  
create table Club(  
	id int not null auto_increment primary key,  
    name text not null,  
    logo text not null  
    );
    
**Maak de tabel Chatbericht**  
create table Chatbericht(  
	id int not null auto_increment primary key,  
    user_id int not null,  
    bericht text not null,  
    tijdstip datetime not null  
    );
    
**Voeg dingen toe aan Chatbericht**  	
insert into Chatbericht (id, user_id, bericht, tijdstip)  
VALUES  
(1, 3, "Hallo gasten", '2016-04-19 13:23:44'),  
(2, 1, "Jo Mannen", '2016-04-19 12:13:45'),  
(3, 2, "Manne zwegt ik zijn naar de shot aan het zien", '2016-04-19 14:28:44'),  
(4, 3, "GODVERDOMME TOKKELIER DAS NA ELKE MATCH HETZELFDE HIEEEEEEE, GISTER WAST OEK AL ZOW", '2016-04-20 05:05:05');  

**Voeg dingen toe aan User**    
insert into User (id, name, functie, email, password)   
values  
(1, "MaartenRAFC", "Admin", "maarten.rafc@hotmail.com", "rafc1180"),  
(2, "TokkeLier", "Voter", "tokkeLIER@lierse.be", "liersetillidie"),  
(3, "Ricardientje", "Voter", "ricardinoAXEL@axelislove.be", "axeltjexxx");  

**Voeg dingen toe aan Votes**  
insert into Votes(id, user_id, club_id, score)  
values  
(1, 1, 1, 3),  
(2, 1, 2, 2),  
(3, 1, 3, 1),  
(4, 2, 2, 3),  
(5, 2, 1, 2),  
(6, 2, 3, 1),  
(7, 3, 1, 3),  
(8, 3, 3, 2),  
(9, 3, 2, 1);  

**Voeg dingen toe aan Club**  
insert into Club(id, name, logo)  
values  
(1, "R Antwerp FC", "test"),  
(2, "Lierse", "test"),  
(3, "KV Mechelen", "test");  


**TESTS**

**Toont de scores van Users**  
SELECT Votes.id, user.name, Club.name, Votes.score  
FROM Votes JOIN User  
ON Votes.user_id = User.id  
JOIN Club   
ON Votes.club_id = Club.id;  

**De chat**  
SELECT User.name, User.functie, Chatbericht.bericht, Chatbericht.tijdstip  
FROM Chatbericht JOIN User  
ON Chatbericht.user_id = User.id  
ORDER BY Chatbericht.tijdstip;  

**Score per club**  
Select  Club.name, SUM(Votes.score)  
From Club Join Votes  
On Club.id = Votes.Club_id  
Group by Club.id;  